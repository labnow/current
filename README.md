# Automation Lab home repo

<!---Add the screenshot of HMI mindmanager chart>--->
<img src="./doc/images/arc.jpg"/>

Automation lab is focus on the DevOps of Testing Automation Tools, mainly for HMI testing.

This repo maintain the docs shared inside DevOps group, and infos & docs shared to outside.

[Release Notes][gitlab-release-link]   |   [Development Plan](#development-plan)   |   [Requirement Analysis](#requirement-analysis)   |   [Known Issues][issues-link]

## Architecture Explanation

Architecture is shown as the title picture, this architecture is testing oriented, which has the initiative to be able to apply on different testing, either HMI testing, or network testing, or ADAS testing ...

With this architecture, the testing team can expand testing competance, increase automation coverage, reduce testing cost, deliver higher-quality service, and other value-added service.

| Stage | Tool | Benefit |
| --- | --- | --- |
| **Pre-Testing** | Test Script Database (On Cloud) <br> Test Script Editor (On Cloud) | Currently, there isn't a tool for testing scripts management & version control, which lead to a bunch of effort wasted. With these tools, testing engineers can design their testing logic, writing & review testing scripts collaboratively on any platform (With web browser). Testing efficiency and quality can be improved. |
| **During-Testing** | Test Execution Dashboard (WebApp) | Currently, there isn't a integrted testing execution panel, testing engineers has to navigate between 3 or 4 different windows on the test bench for test execution. With this WebApp, test execution can be controlled & managed in a single WebApp on any device, laptop or ipad ... |
| | Automation testing middle platform (Middleware) | Currently, most of the testing projects on-going is executed manually with some independentt tools. <br> Using the Middleware concept, we plan to build this testing middle platform, which has the basic module of `Test Case， Report and Log, Keywords, Library, Template, Task.`  Differnet type of testing projects can run as microservice on this middleware, which can be create using `Template`, and share the core function module. After researching and discussion, we found **Multimedia automated testing template, Basic function automated testing template, Diagnostic/Refresh automated testing template, Light intensity automated testing template, Aging stress automated testing template** are worth to be created.  <br> It has a built-in library of common tools needed for testing, also, integrate new tools via API interface is possible.  |
| | Tools | **Computer Vision**: Replace human in UUT output assessment, will improve automation coverage & speed testing execution. <br> **Voice interaction**: Replace human is triggering UUT & assessing voice output of UUT, will increase automation coverage, quality & speed up testing. <br> **Multi-degree robot arm**: the existing robot arm is too expensive and can not fullfill requirement. <br> **Camera System**: An upgrade add-on for robot arm. <br> **Electronic load (dummy load)**: can replace the read load needed for testing, will simplify the setup of testing environment. <br> **Programmable potentiometer**: need this tool in testing, but currently we don't have. <br> **Bus communication tools (CAN/LIN, etc.)**: Can replace CANoe (15K RMB), saving testing cost. <br> **Bus interference tool**: Replace CANstress, saving testing cost. |
| **Post-Testing** | Testing result database <br> Data analysis of testing result | Current testing result are separated files of excel/html. With testing result database, testing team is able to gather all kind of testing result together, based on this dataset, we can build data analytic models to derive testing insight & defect analysis of unit been tested, and other value added service. |


## General Status

> This section will be moved to a stadalone page.

| Tool | Language | Platform | Development | Integration |
| --- | --- | --- | --- | --- |
| **Computer Vision** |
| - Pattern Match | Python | windows | 20% | 0% |
| - Locate Pattern | | | 0% | 0% |
| - OCR |
| **Voice Interaction** |
| - TTS | Python | linux | 10% | 0% |
| - Voice Recognition | 
| **HiL** |
| - Integrate NI Instrument | LabView | | In-progress |
| - SpeedGoat | | | No-Hardware |
| **Test Environment** |
| - Test Script Editor | Script/Excel |
| - Execution dashboard | | Docker |
| **Management Tool** |
| - Script Database | | Docker |
| - Test result database |
| - Test data analysis | Python |

## Tool Explanation

### Computer Vision

Successor of GTS. WIll support following features, Pattern Match - Match target Pattern from [static image or video stream](#video-capture) of UUT (Unit Under Testin), Locate pattern (return X,Y coordinate), OCR.

#### Pattern Match

[Pattern match](gitlab-404) should be able to find target pattern on given image, and return ?find=Yes/No, coordinate of pattern, timestamp (For smoothness testing) of found pattern. 

### Video Capture

This module capture image/video from UUT, and save image/streaming video for computer vision module. Depend on target device, below are some of the possible method.

| Target Device | Method | Dependency |
| --- | --- | --- |
| Android | adb shell screencap -p | Android Developer Mode, USB Port |
| Has secondry video port | epipan streaming card |
| Non-touch screen | video streaming card | |
| Serized video signal | LVDS to HDMI Card | Need 3+ months for developing |
| Linux/QNX MMI | | Yet studied | 
| Any | camera | HD Camera + Algorithm | 

### Voice Interaction

[Voice Interaction](gitlab-404) is the module which support 1. playback recorded voice memos, generate voice from text (TTS) to trigger UUT. 2. Recognition voice from UUT, and perform assessment.

### Test Script Editor

[Test Script Editor](gitlab-404) is the tool for test engineers to read/write/edit test scripts, now is in concept phase.


| End Client | + | - |
| --- | --- | --- |
| Excel | User friendly | No Syntax check, proofing |
| VS Code | Cross Plateform, can run in a single browser. <br> Syntax Check, proofing extension <br> Test script management at single line of script level| Take time for beginer to learn |

### Test execution & real-time dashboard

Successor of ATS, will support:
- A control panel for test execution.
- Test execution monitoring dashboard.

> For cross platform compatibility, will try build as WebApp.

### Script Interpreter

Script interpreter is the module which read test script (in structural phrase, such as, `COMMAND -flag --option`), call specific execuable package and store the return value from command executed.

### HiL

HiL, will responsible for signal simulation for UUT, fault injection test, currently using cards from NI.

#### SpeedGoat

> SpeedGoat can replace NI cards, will research later.

## Development Plan

>Insert Notion.io Chart ?or Mindmanager Chart

## Requirement analysis

Competitor benchamrking and requirement insight from market will be maintained by marketing team.

[gitlab-release-link]: https://gitlab.com/labnow/current/-/releases/
[issues-link]: https://gitlab.com/labnow/current/-/blob/master/issue%231.md
[gitlab-404]: https://gitlab.com/404.html
