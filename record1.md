# Record #1 by XuGang

In this series, i will rewind previous works, make following plan, propose initiatives for seniors to review. 

This record will be sent out periodically.

## Summary of previous work

### Leading 

| Item | Topic | 
| --- | --- |
| Manage technical aspect of current HMI testing solution, include <br> *Customer requirement analysis, improvement for testing system, system demonstration*  | V&V |
| Development activity within automation lab <br> [*This page shows detailed development activity & status*][home-repo] | V&V |

### Support

| Item | Effort | Topic | 
| --- | --- | --- |
| Project Management Support <br> *Including: IT Support, Document Translation, ad-hoc issue resolving at customer site, Project Data Management*| 6 Hours/Week| V&V |
| Following-up & Support Managing GAC Complex Driver Development Project | 6 Hours/Week | Base SW |
| Technical support for Rad's digital projects, include: <br> *Homologation --> Find applicable HW/SW, such as, Cloud service provider, PLCs, Network Devices ... --> Find feasible local vendor --> Make procurement plan, ask for demo/trial.* <br> *Technical proposal proofing & demostation; Benchmarking; Market Analysis* | 6 Hours/Week | Digital |

## Future plan & Initiatives

Will focus on the test automation software/system DevOps, which will expand the testing competence of current team, increase the automation coverage (*<20% for HMI testing currently*) for V&V projects, subsequently reduce project execution cost, speed-up project exectution, deliver higher-quality service to customer, and bring value-added service, *e.g. data anlysis for testing result.* <br> [This page describes detailed plan, roadmap of development.][home-repo]

### Initiatives

| Problem we faced | Initiative | Benefit | Effort expectation |
| --- | --- | --- | --- |
| No document management & Version control tool for almost all ongoing projects. <br> *In some meeting, when someone refer to a specific document, it will take quite a while for the other person to find out that document, normally they can only find it in outlook. This lead to a bunch of time wasted.* <br> *During either technical proposal stage, or requirement analysis stage, there will be serveral version of a single document. Since there is no version control tool, it leads to a lot of repeating work, e.g. Has to translate the whole document when just some items has been updated.(>40% of translation has this issue* | Deploy GIt Tool for document management and version control | Reduce null repeating work. <br> Reduce the document-forwarding-using-email work of project managers. <br> Better change log, issue follow-up. | 2 Weeks |
| Currently, most projects are using Excel sheet to follow issues, tracking open point. <br> *Should using email to notif the other side when some item is updated --> Time wasting* <br> *No logs --> Hard to Track changes* | Delpoy Redmine for project managements | Advanced & easy-to-use Project Management System, put all resource needed for project management in one place, can being access with web portal, can integrate with Git. | 3 Weeks |
| Does not have enough staff for doing document translation | Deploy Computer Assistant Translation Software | As estimted, translation efficiency can be improved by 70%. <br> Efficiency will keep going up with translation database growing. | 2 Days |

[home-repo]: https://gitlab.com/labnow/current/