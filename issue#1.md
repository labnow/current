# ISSUE #1 HMI Automated Testing System issues summary 20201118

**Conclusion:** 
1. Current HMI Automation test setup is not able to be delivered directly to customer.
2. Considering the current developement progress (20201118), and the resources put on HMI project, we probally can not deliver a copy of the current HMI testing setup to customer with >20% automation coverage.

[Defect Analysis](#defect-analysis) | [Improvement Plan](#improvement-plan) | [Developmment Status](#development-status)

## Defect Analysis
Summary of the current HMI automation testing setup --> ATS + GTS + NI HW

- **Ease of Use:**  Not user friendly. 
  - Test script writing <br> When create a new pattern match object, Engineer has to navigate between 3 or 4 different windows. GTS can easily lag when a large region is selected fot pattern training.
  - Learning effort <br> 2+ weeks is needed for a engineer with basic programming competance and testing experince. *⚠ There isn't any system level training materials.*
  - Testing resource management <br> Does not have a specific testing files management utility, testing engineers can easily get confused between large numbers of `Tags`, `Profiles`.
  - Collaborative Testing <br> Can not have muiltiple setups working on a common project --> can't share one common test resource library or one common test script library. *⚠ From our use experience, it is not easy to distribute testing task to different engineers, that is to say, the test scripts writing and test resource library creating (Train Pattern) shall be done by the same person, otherwise will cause confusion.*
- **Extensibility:** Can not integrate new tool/sofware/hardware into current setup by common method. 
- **Automation Coverage:** 20%
- **System Reliability:** <br> Low, if current setup is delivered to customer, a maintenance engineer should be sent to customer site for troubshooting and quick repair.
- **Support & TroubleShooting:** <br> Current does not have rbei indian engineer support on this system. So that, issues we have came across is effort-costing, and most of them can not be solved by us. 

### Complete issues list

> Add the problem_issues excel sheet here

### Technical Deviation

> Add the technical deviation from customer requirement (excel sheet) here

> Add HMI.mmap here

<!--- ATS + GTs + Robotic Arm (Indian Made) + Camera (Not able to capture clear UUT's screen for testing). --->

## Improvement Plan

Based on the experience of using this HMI Automated Testing setup (ATS+GTS+Power Supply VI) and comparison it with NI Software suite, we think that the current setup is **Secondry developped based on NI Software suite**. 

The ATS is a tidy version of NI TestStand, the GTS is a GUI program which has NI Vision running behind it, the Programable Power Supply program is created based on labview template (which won't response after first call -> if want to use it again, should kill the process in task manager then re-execute this program) ... 

So, we have the assumption that, RBEI india themselves is not able to integrate any 3rd party software/tool (Non NI) to the testing system. Moveover, the only API connection we can found in this whole system is the command forwarding from ATS -> GTS, for other cases where infomation transfer between software is needed, they relized the "API" function by using txt file. 

All above, we think probably rbei india is not able to provide us with the APIs/Interface we needed for developing new features. Only if, the architecture of HMI Testing System has been re-designed after April 2019 --> *The build date of ATS & GTS on our rack is April 2019*  

Upon the discussions we have had internally, there are three roadmaps can take, shown as below.

[Deliver to customer as is](#deliver-as-is) | [Improve based on current setup](#improve-current-setup) | [Use alternative software](#use-alternative-software)

### Deliver to customer as is

**The Testing setup we deliver**

- Inherit all the [defects](#defect-analysis) of current HMI Automated Testing setup.
- Cause rbei india has not yet tell us how to install all those software of current setup (emailed months ago, no reply), we are not able to create a copy of the current testing setup to deliver to customer.

**Effort we need to put for HMI projects**

- Instructor for teaching engineers from customer on how to use this whole setup, cause currently there isn't any learning materials of the level of the entire testing system. There is only hard-to-understanding  & uncompleted manual (english version) of ATS & GTS. New testing engineers is not able to learn by themselves with these 2 manuals, otherwise, they have to learn by guess.
- A maintenance engineer should be sent to customer side, who should have basic networking and software knowledge, from our experience, connection (tcp/ip) loss between GTS <-> Robotic Arm may happen after 30 minutes of continuous test execution, frame capture module of GTS may not response after 1 ~ 2h of continuous test execution.

### Improve based on current setup

To choose this roadmap, we assumed that for the following improvement/development, we will reuse the core part of current setup --> ATS & GTS

**The Testing setup we deliver**

- Inherit all the [defects](#defect-analysis) of ATS & GTS.
- Can send command to run NI card's software, but not able to get any data from NI Cards.

**Improve Method**

| Improvement Item | Possible Method |
| --- | --- |
| Using exsting NI Cards, such as, sound & wave, RF ... | Write a standalone labview windows program which encapsulated with STDIN interface --> `***.exe -flag --option`, when using certain NI card, call specific VI from CMD. By this method, API interface can only pass several variable and can only pass once at program startup. | 
| Voice Interaction | No Idea |
| Robotic Arm | Still using .txt file |  

### Use alternative software

By chosen this roadmap, we can integrate a system with either self-written software/tool or 3rd party software/tool or freesoftware.

Such as, OpenCV to replace GTS, RobotFrame to replace ATS. *(Both are open source)*

Manageable and customizable.

**Improve Method**
> Depend on requirement

## Development Status

Current there are two engineer (Sun & Gang) working on the development of this hmi automation testing system projrct.

| Engineer | Achievement if(succeed/stuck/ongoing) ?why|
| --- | --- |
| 70% of Sun <br> *Other 30% effort of Sun is occupied by requirement analysis & document writing* | \+ Understading --> know well --> provide improvement suggestion of this HMI setup (succeed) <br> \+ find alternative camera system (stuck) ?=Not able to apply trial from camera vendor <br> \+ figure out --> write labview code to use exisiting NI HW on rack (ongoing) <br> \+ study on the solution of LVDS2HDMI card (stuck) ?=development will take 3+ months, can not 100% focus on the development of this card <br> \+ sourcing vendors according to improvement plan (ongoing) <br> \+ To be add by Sun himself ... |
| 30% of Gang <br> *Other 70% effort of Gang is occupied by PM and other work* | \+ bug fix (ongoing) <br> \+ develop [computer vision](README.md/#computer-vision) software based on OpenCV as alternative for GTS (stuck) ?=no hw to test developed algorithm <br> \+ ADB method for testing android MMI (succeed) <br> \+ research on using SpeedGoat as alternative for NI Hardware (stuck) ?=no related hardware <br> \+ r&d on robotic arm (ongoing) <br> \+ research on alternative of ATS, shell or python (ongoing) <br> \+ cross platform test script editor (ongoing) |

### Problems within development team
- When there is a choice need to be made (e.g. choose camera model/how to get development kit), none of us is able to make the decision, this cause stuck in many work.
- None of us can continuesly put most effort on the r&d of a single part/module, this lead to low efficiency and can hardly build any sophisticated software.
- None of us has a clear idea of the general direction/plan of the HMI project *"how much resource, effort will be put on this project? the general timeline?"* without this, it is hard to choose development roadmap, hard to make meticulous development plan, eventually we will end up with some pieces of software/application, not as a whole.
  